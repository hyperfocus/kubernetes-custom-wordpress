PW=test
ZONE=asia-east1-a


# Secrets
secrets: create-password remove-newline create-secret

create-password:
	echo "${PW}" >> password.txt

remove-newline:
	tr --delete '\n' <password.txt >.strippedpassword.txt && mv .strippedpassword.txt password.txt

create-secret:
	kubectl create secret generic mysql-pass --from-file=password.txt


# Disk
create-persistent-disks: create-disk-1 create-disk-2 register-disk

create-disk-1:
	gcloud compute disks create --size=20GB --zone=${ZONE} wordpress-1

create-disk-2:
	gcloud compute disks create --size=20GB --zone=${ZONE} wordpress-2

register-disk:
	kubectl create -f gce-volumes.yaml


# Create
create: create-mysql create-wordpress

create-mysql:
	kubectl create -f mysql-deployment.yaml

create-wordpress:
	kubectl create -f wordpress-deployment.yaml


# Delete
delete: delete-deployment delete-secret delete-pvc delete-gce-pv

delete-deployment:
	kubectl delete deployment,service -l app=wordpress

delete-secret:
	kubectl delete secret mysql-pass

delete-pvc:
	kubectl delete pvc -l app=wordpress

delete-local-pv:
	kubectl delete pv local-pv-1 local-pv-2

delete-gce-pv:
	kubectl delete pv wordpress-pv-1 wordpress-pv-2
